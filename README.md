# Node Blog

## Implementation d'un Blog

Utilisation des technos suivante : 

    - Node
    - Express
    - MongoDb (Atlas)
    - Heroku
    - Mongoose

## A propos

Prise en main de Node Express et MongoDB. Utilisation de Mongoose.
Mise en place d'un CRUD. 

## Projet heberge via Heroku

[Node-Blog](https://node-blog-pierre.herokuapp.com)


