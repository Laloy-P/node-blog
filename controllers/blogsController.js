//blog-index, blog detail, blog-create-get, blog-create-post, blog-delete
const Blog = require ('../models/blogs');

const blog_index = (req, res ) => {
    Blog.find().sort({createdAt: -1})
    .then((result)=>{ res.render('blogs/index', {title:'All blogs', blogs: result}) })
    .catch((error)=>{ console.log(error) });
}

const blog_detail = (req, res) => {
    const id = req.params.id;
    console.log(id);
    Blog.findById(id)
        .then((result) => { res.render('blogs/details', {blog: result, title: 'Blog details'}) })
        .catch((error) => { res.status(404).render('404', {title: 'Article introuvable.'}) });
}

const blog_create_get = (req, res) => {
    res.render('blogs/create', {title: 'create a new blog'});
}

const blog_create_post = (req, res) => {
    const blog = new Blog(req.body);
    blog.save()
        .then((result) => { res.redirect('/blogs') })
        .catch((error) => { console.log(error) });
}

const blog_delete = (req, res) => {
    const id = req.params.id
    Blog.findByIdAndDelete(id)
        .then((result) => { res.json({redirect: '/blogs'}) })
        .catch((error) => { console.log(error) });
}

module.exports = {
    blog_index,
    blog_detail,
    blog_create_get,
    blog_create_post,
    blog_delete
}