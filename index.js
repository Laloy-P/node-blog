const express = require('express');
const morgan = require('morgan');
const mongoose = require ('mongoose');
const blogRoutes = require('./routes/blogRoutes');
require('dotenv').config()

//Express app
const app = express();
const port = process.env.PORT || 3000;

//Mogodb Credential
const CLUSTER_NAME = process.env.CLUSTER_NAME;
const DB_NAME = process.env.DB_NAME;
const DB_USER = process.env.DB_USER;
const DB_PWD = process.env.DB_PWD;

//Connect to mongodb
const dbUri = `mongodb+srv://${DB_USER}:${DB_PWD}@${CLUSTER_NAME}.wtnwt.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`;
mongoose.connect(dbUri, {useNewUrlParser: true, useUnifiedTopology: true})
    .then((result)=> {
        app.listen(port);
        console.log(`L'application s'execute sur le port ${port}`)
    })//on attend la conection a la bdd avant de lancer le serveur
    .catch((error)=> console.log(error));

//Register view engine
app.set('view engine', 'ejs');

// middleware & static files
app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));
app.use(morgan('dev'));

//Routes
app.get('/', (req, res) => {
    res.redirect('/blogs');
});

app.get('/about', (req, res) => {
    res.render('about', {title: 'about'});
});

//Blog routes
app.use('/blogs',blogRoutes);

//404 use se declenche pour tout les type de requete il faut donc le placer en dernier il faut specifier le status egalement
app.use((req, res)=>{
    res.status(404).render('404', {title: '404'});
});